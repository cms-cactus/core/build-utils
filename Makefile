
PACKAGE_VERSION=0.3.5
PACKAGE_RELEASE=0

CACTUS_ROOT=/opt/cactus

PACKAGE_DIR=$(shell pwd)
RPMBUILD_DIR=$(shell pwd)/rpm/RPMBUILD

.PHONY: rpm _rpmbuild
rpm: _rpmbuild

_rpmbuild: 
	mkdir -p ${RPMBUILD_DIR}/{RPMS/{i386,i586,i686,x86_64},SPECS,BUILD,SOURCES,SRPMS}
	rpmbuild --quiet -bb -bl --buildroot=${RPMBUILD_DIR}/BUILD \
	  --define "_topdir ${RPMBUILD_DIR}" \
	  --define "_packagedir ${PACKAGE_DIR}" \
	  --define "_version ${PACKAGE_VERSION}" \
	  --define "_release ${PACKAGE_RELEASE}" \
	  --define "_prefix ${CACTUS_ROOT}" \
	  cactuscore-build-utils.spec
	find  ${RPMBUILD_DIR} -name "*.rpm" -exec mv {} $(PACKAGE_DIR)/rpm \;

.PHONY: cleanrpm _cleanrpm
cleanrpm: _cleanrpm
_cleanrpm:
	rm -rf rpm
