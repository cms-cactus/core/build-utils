BUILD_UTILS_DIR = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
RPMBUILD_DIR=${PackagePath}/rpm/RPMBUILD


BUILD_REQUIRES_TAG = $(if ${PackageBuildRequires} ,BuildRequires: ${PackageBuildRequires} ,\# No BuildRequires tag )
REQUIRES_TAG = $(if ${PackageRequires} ,Requires: ${PackageRequires} ,\# No Requires tag )


.PHONY: rpm _rpmall
rpm: _rpmall
_rpmall: _all _spec_update _rpmbuild

.PHONY: _rpmbuild
_rpmbuild: _spec_update
	mkdir -p ${RPMBUILD_DIR}/{RPMS/{i386,i586,i686,x86_64},SPECS,BUILD,SOURCES,SRPMS}
	rpmbuild --quiet -bb -bl --buildroot=${RPMBUILD_DIR}/BUILD --define  "_topdir ${RPMBUILD_DIR}" rpm/${PackageName}.spec
	find ${RPMBUILD_DIR} -name "*.rpm" -print0 | xargs -0 -n1 -I {} mv {} rpm/

.PHONY: _spec_update	
_spec_update:
	mkdir -p ${PackagePath}/rpm
	cp ${BUILD_UTILS_DIR}/specTemplate.spec ${PackagePath}/rpm/${PackageName}.spec
	sed -i -e 's#__package__#${Package}#' \
	       -e 's#__packagename__#${PackageName}#' \
	       -e 's#__version__#$(PACKAGE_VER_MAJOR).$(PACKAGE_VER_MINOR).$(PACKAGE_VER_PATCH)#' \
	       -e 's#__release__#${PACKAGE_RELEASE}.${CACTUS_OS}#' \
	       -e 's#__prefix__#${CACTUS_ROOT}#' \
	       -e 's#__packagedir__#${PackagePath}#' \
	       -e 's#__os__#${CACTUS_OS}#' \
	       -e 's#__project__#${Project}#' \
	       -e 's#__author__#${Packager}#' \
	       -e 's#__summary__#None#' \
	       -e 's#__description__#None#' \
	       -e 's#__url__#None#' \
	       -e 's#__includedirs__#$(Includes)#' \
	       -e 's|^.*__build_requires__.*|${BUILD_REQUIRES_TAG}|' \
	       -e 's|^.*__requires__.*|${REQUIRES_TAG}|' \
	       -e 's|__buildutils_dir__|${BUILD_UTILS_DIR}|' \
	       ${PackagePath}/rpm/${PackageName}.spec

.PHONY: cleanrpm _cleanrpm
cleanrpm: _cleanrpm
_cleanrpm:
	-rm -r rpm

