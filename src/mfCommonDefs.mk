
# Sanitize BUILD_HOME
BUILD_HOME := ${shell cd ${BUILD_HOME}; pwd}

$(info Using BUILD_HOME=${BUILD_HOME})

export BUILD_HOME

CACTUS_ROOT ?= /opt/cactus

# OS identification
CACTUS_PLATFORM=$(shell if [ -f /etc/system-release ]; then cat /etc/system-release; fi)
CACTUS_OS=unknown.os
UNAME=$(strip $(shell uname -s))
ifeq ($(UNAME),Linux)
    CACTUS_OS_EL_MAJOR=$(shell cat /etc/system-release | sed 's/^\(CentOS Linux\|CentOS Stream\|AlmaLinux\|Rocky Linux\) release \([0-9]\).*/\2/;t;d')
    ifneq ($(CACTUS_OS_EL_MAJOR),)
        CACTUS_OS=el$(CACTUS_OS_EL_MAJOR)
    endif
else ifeq ($(UNAME),Darwin)
    CACTUS_OS=osx
endif

$(info OS detected: $(CACTUS_OS))

# XDAQ config
XDAQ_ROOT ?= /opt/xdaq
# XDAQ bug
# A new internal variable in XDAQ makefiles
# This default value is set in mfAutoconf.rules
# at the end of the file, after trying to use it
BUILD_SUPPORT?=build


## Environment
# Make sure $CACTUS_ROOT/lib and $XDAQ_ROOT/lib are present in LD_LIBRARY_PATH

ifeq ($(findstring $(CACTUS_ROOT)/lib,$(LD_LIBRARY_PATH)),)
    $(info CACTUS_ROOT/lib added to LD_LIBRARY_PATH)
    LD_LIBRARY_PATH:=$(CACTUS_ROOT)/lib:$(LD_LIBRARY_PATH)
else
    $(info CACTUS_ROOT already in LD_LIBRARY_PATH)
endif

ifeq ($(findstring $(XDAQ_ROOT)/lib,$(LD_LIBRARY_PATH)),)
    $(info XDAQ_ROOT/lib added to LD_LIBRARY_PATH)
    LD_LIBRARY_PATH:=$(XDAQ_ROOT)/lib:$(LD_LIBRARY_PATH)
else
    $(info XDAQ_ROOT already in LD_LIBRARY_PATH)
endif

export LD_LIBRARY_PATH

#$(info Linker path: $(LD_LIBRARY_PATH))
VERBOSE ?= 0
DEBUG ?= 1

# Compilers
CPP:=g++
LD:=g++

## Tools
MakeDir=mkdir -p

# Formatting
At_0 := @
At_1 := 
At = ${At_${VERBOSE}}

ColorGreen := \033[0;32m
ColorNone := \033[0m

CPP_Actual := ${CPP}
CPP_0 = @echo -e "-> Compiling ${ColorGreen}$(<F)${ColorNone}..."; ${CPP_Actual}
CPP_1 = ${CPP_Actual}
CPP = ${CPP_${VERBOSE}}

LD_Actual := ${LD}
LD_0 = @echo -e "-> Linking ${ColorGreen}$(@F)${ColorNone}..."; ${LD_Actual}
LD_1 = ${LD_Actual}
LD = ${LD_${VERBOSE}}

MakeDir_Actual := ${MakeDir}
MakeDir = ${At} ${MakeDir_Actual}

## Python
PYTHON ?= python3.6
PYTHON_VERSION ?= $(shell ${PYTHON} -c "import distutils.sysconfig;print(distutils.sysconfig.get_python_version())")
PYTHON_INCLUDE_PREFIX ?= $(shell ${PYTHON} -c "import distutils.sysconfig;print(distutils.sysconfig.get_python_inc())")

# Compiler flags
ifndef DEBUG
    CxxFlags = -g -Wall -Werror=return-type -O3 -MMD -MP -fPIC
    LinkFlags = -g -shared -fPIC -Wall -O3 
    ExecutableLinkFlags = -g -Wall -O3
    # For XDAQ
    UserCCFlags = -g -Wall -Werror=return-type -O3 -MMD -MP -fPIC
    UserStaticLinkFlags = -g -shared -fPIC -Wall -O3
    UserDynamicLinkFlags = -g -shared -fPIC -Wall -O3
    UserExecutableLinkFlags = -g -Wall -O3 -std=c++11
else
    CxxFlags = -g -ggdb -Wall -Werror=return-type -MMD -MP -fPIC
    LinkFlags = -g -ggdb -shared -fPIC -Wall
    ExecutableLinkFlags = -g -ggdb -Wall -std=c++11
    # For XDAQ
    UserCCFlags = -g -ggdb -Wall -Werror=return-type -MMD -MP -fPIC
    UserStaticLinkFlags = -g -ggdb -shared -fPIC -Wall
    UserDynamicLinkFlags = -g -ggdb -shared -fPIC -Wall
    UserExecutableLinkFlags = -g -ggdb -Wall -std=c++11
endif

ifeq (${CACTUS_OS},el7)
    CxxFlags += -std=c++11
    # For XDAQ
    UserCCFlags += -std=c++11
endif

