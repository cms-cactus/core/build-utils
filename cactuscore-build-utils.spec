%define _tmppath /tmp
#%define _buildarch __buildarch__
#%define _includedirs __includedirs__

Name: cactuscore-build-utils
Version: %{_version}
Release: %{_release}
BuildArch: noarch
Packager: Alessandro Thea, Tom Williams
Summary: Configuration files for building CACTUS packages - e.g. common makefile variables/rules.
License: BSD License
Group: CACTUS
Source: https://gitlab.cern.ch/cms-cactus/core/build-utils
URL: https://cactus.web.cern.ch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
Prefix: %{_prefix}

%description
Configuration files for building CACTUS packages - e.g. common makefile variables/rules.


#%prep

#%build

%install 

mkdir -p $RPM_BUILD_ROOT/%{_prefix}/build-utils
cd %{_packagedir}/src; \
find . -name "*" -exec install -D -m 644 {} $RPM_BUILD_ROOT/%{_prefix}/build-utils/{} \;
chmod a+x $RPM_BUILD_ROOT/%{_prefix}/build-utils/install.sh

%clean

%post 

%postun 

%files 
%defattr(-, root, root, -) 
%{_prefix}/build-utils
